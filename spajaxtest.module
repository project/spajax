<?php


// spajaxtest.module for Drupal by Jeff Robbins

/**
 * @abstract 
 * A number of test pages to show what the Scriptaculous/Prototype/AJAX (spajax)
 * module is capable of and how the code works
 */

/**
 * Test callback page with examples of how to use the code
 *
 */
function spajaxtest_test(){
  if ($_POST['edit']){
    $sorted = spajax_get_sort($_POST['edit']['sort']);
    $output = "<p>Sortable order: ". implode(', ', $sorted) ."</p>";
    return $output;
  }
  $output .= '<h1>Sorting</h1>';
  $output .= "<p>Sortable lists with the ability to move items from one list to the other.</p>";
  $items1 = array('item 1', 'item 2', 'item 3', 'item 4');
  $options = array('containment' => array('list1', 'list2')); // items can be moved between these lists
  $output .= theme_sortable_list($items1, 'List One', $options, array('id' => 'list1'));
  $items2 = array('item A', 'item B', 'item C');
  $output .= theme_sortable_list($items2, 'List Two', $options, array('id' => 'list2'));

  $output .= '<h1>Drag and Drop</h1>';
  $output .= "<div id='dragbox' class='draggable' style='border:1px solid #666;background-color:#DDD;width:100px;height:100px;'>This box is draggable.</div>";
  spajax_draggable_element('dragbox');
  
  $output .= '<h1>Simple JS Link</h1>';
  $output .= '<p>'.spajax_link_to_function("Greeting", "alert('Hello world!')").'</p>';
  
  $output .= '<h1>Effects</h1>';
  $output .= '<div>'.spajax_link_to_function('blind up', spajax_visual_effect('BlindUp', 'blindbox')).'</div>';
  $output .= '<div>'.spajax_link_to_function('blind down', spajax_visual_effect('BlindDown', 'blindbox')).'</div>';
  $output .= "<div id='blindbox' class='blind' style='border:1px solid #666;background-color:#DDD;width:100px;height:200px;'><p>Box full of content.</p><p>Blah blah</p><p>Blah</p></div>";
  
  $output .= "<h2>Slider Controls</h2>";
  $output .= '<div id="track1" style="width:200px;background-color:#aaa;height:5px;">
    <div id="handle1" style="width:5px;height:10px;background-color:#f00;"> </div>
  </div>
  
  <p id="debug1"> </p>';
  // notice that the keys for functions are prefixed with #
  // these functions also use Prototype's $() shorthand command to locate the elements
  $options = array(
    'sliderValue' => 0.5,
    '#onSlide' => "function(v){\$('debug1').innerHTML='slide: '+v}",
    '#onChange' => "function(v){\$('debug1').innerHTML='changed! '+v}"
    );
  spajax_slider('handle1', 'track1', $options);    
  
  /* This isn't working
  $output .= "<h2>Dynamic Elements (in progress)</h2>\n";
  // problems:
  // is it possible to get just *part* of a form?
  
  $form['#tree'] = true;
  $form['input'] = array('#type' => 'textfield', '#title' => 'email');
  // generating error...
  _form_builder('foo', $form); // this will populate the form array so there are no errors
  //$form['#printed'] = false;
  $html = form_render($form);
  spajax_create_function('add_email_field', spajax_insert_html('dynform', $html, 'Bottom'));
  $output .= spajax_link_to_function('add another field', 'add_email_field()');
  $output .= "<div id='dynform'>\n$html\n</div>";
  */
  
  return $output;
}

function spajaxtest_inplace_test($op = NULL){
  if ($op == 'callback'){
    if($val = $_POST['value']){ // prototype defaults to sending content using this variable
      variable_set('spajax_inplace_test', $val);
      print $val;
    }
    exit;   
  }
  
  $output .= "<p>Scriptaculous has an in-place editor that allows you to edit content without reloading the page. Click in the next paragraph to edit it. Submit your content. It will be saved and appear on this page in the future.</p>";
  
  $output .= "<div id='editable'>";
  $output .= variable_get('spajax_inplace_test', 'You can edit this text!');
  $output .= "</div>";
  
  spajax_in_place_editor('editable', 'spajax/test/inplace/callback', array('rows' => 5));
  
  return $output;
}

function spajaxtest_sortable_test(){
  if ($edit = $_POST['edit']){
    $order = spajax_get_sort($edit['sort']);
    drupal_set_message('Order: '. implode(', ', $order));
  }
  
  $output .= "<p>Scriptaculous has a the ability to make elements on the page sortable.</p>
  <p>The spajax.module extends this to create a new form element type called 'sortable'. The order of the elements are stored in a hidden field and submitted with the form.</p>
  <p>Sort the following, then submit.</p>";
  $items = array('first' => 'This one was first', 'second' => 'This one was second', 'third' => 'This one was third');
  //$options = array('#onUpdate' => "function(){alert(Sortable.serialize('formsort'));}");
  $form['sort'] = array('#type' => 'sortable', '#items' => $items, '#title' => t('Sortable'), '#attributes' => array('id' => 'formsort'));
  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  $output .= drupal_get_form('sort_test', $form);
  return $output;
}

/**
 *  ************************ Drupal Hooks **************************
 */

/**
 * Implementation of hook_menu()
 *
 */

function spajaxtest_menu($may_cache){
  $items = array();
  if ($may_cache) {   
    // test stuff
    $items[] = array('path' => 'spajax/test', 'access' => TRUE, 'title' => 'spajax test', 'callback' => 'spajaxtest_test', 'type' => MENU_NORMAL_ITEM | MENU_EXPANDED);
    $items[] = array('path' => 'spajax/test/inplace', 'access' => TRUE, 'title' => 'in place editing', 'callback' => 'spajaxtest_inplace_test');
    $items[] = array('path' => 'spajax/test/sortable', 'access' => TRUE, 'title' => 'sortable form element', 'callback' => 'spajaxtest_sortable_test');
  }
  return $items;
}

function spajaxtest_help($section){
  switch($section){
    case 'admin/modules#description':
      return t('Test code for the SPAjax module. Provides a few pages to show what can be done.');
  }
}