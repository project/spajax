-----------------------------------------------------------
S/P Ajax for Drupal
  by Jeff Robbins
    jeff -/a@t\- lullabot.com


Description: ----------------------------------------------
This module implements Scriptaculous/Prototype AJAX functions into Drupal. The spajax.module itself only provides for animated Drupal status messages, but other modules will build on the API provided by this module.

Be sure to check out the S/P Magic Menus module which creates dynamic, expandable, and floating menus from Drupal's menu blocks.

Installation: ---------------------------------------------
Due to licensing difference between Drupal and Scriptaculous/Protoype,

*** YOU NEED TO DOWNLOAD THE SCRIPTACULOUS LIBRARY SEPERATELY. ***

1) Put the directory that contains this file into your Drupal modules directory.

2) Download and expand the Scriptaculous library from http://script.aculo.us/downloads (this download includes the Prototype library)

3) Rename the downloaded directory "scriptaculous"

4) Place the directory in the directory that contains this file.

Your directory will look like this

[drupal]/
  index.php
  [other files]
  modules/
    spajax/
      spajax.module
      spajax.js
      spajax.css
      README.txt
      scriptaculous/
        lib/
        src/
        README
        [other files]
        
 5) Enable the module at admin/modules
 
 6) Go to admin/settings/spajax to set it up...