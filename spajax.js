function float_menu(menu_id, pos, top_pos, left_pos, callback){
  menu = $(menu_id);
  if(pos != 'normal'){
    // set up positioning and draggability
    Element.addClassName(menu, 'floatmenu');
    menu.style.position = pos;
    document.body.appendChild(menu);
    handle = document.getElementsByClassName('title', menu_id)[0];
    new Draggable(menu_id, {handle:handle});
    Element.addClassName(handle, 'draggable');
    Event.observe(handle, 'mouseup', function(){
      new Ajax.Request(callback, { method: "post", postBody: 'vars['+menu_id+'][left]='+ $(menu_id).style.left +'&vars['+menu_id+'][top]='+ $(menu_id).style.top });
    });
    $(menu_id).style.top = top_pos;
    $(menu_id).style.left = left_pos;
  }
  expandeds = document.getElementsByClassName('expanded', menu_id);
  for (i=0; i<expandeds.length; i++) {
    subul = expandeds[i].getElementsByTagName('ul')[0];
    firsta = expandeds[i].getElementsByTagName('a')[0];
    firsta.myli = expandeds[i];
    firstaclone = firsta.cloneNode(true);
    subactive = document.getElementsByClassName('active', expandeds[i]).length;
    if (!subactive) {
      Element.hide(subul);
      Element.addClassName(firsta, 'collapsed');
      Element.removeClassName(expandeds[i], 'expanded');
      Element.addClassName(expandeds[i], 'collapsed');
    }
    else {
      Element.addClassName(firsta, 'expanded');
    }
    liclone = expandeds[i].cloneNode(false);
    Element.removeClassName(firsta, 'active');
    firsta.setAttribute('onclick', 'this.blur(); return false;');
    liclone.className = 'leaf';
    liclone.appendChild(firstaclone);
    lis = subul.getElementsByTagName('li');
    subul.insertBefore(liclone, lis[0]);
    Element.addClassName(lis[lis.length - 1], 'last');
    firsta.flip = function() {
      myul = this.parentNode.getElementsByTagName('ul')[0];
      vis = Element.visible(myul);
      Element.removeClassName(this, vis ? 'expanded' : 'collapsed');
      Element.addClassName(this, vis ? 'collapsed' : 'expanded');
      Element.removeClassName(this.myli, vis ? 'expanded' : 'collapsed');
      Element.addClassName(this.myli, vis ? 'collapsed' : 'expanded');
      new Effect.toggle(this.parentNode.getElementsByTagName('ul')[0], 'blind');
    }
    Event.observe(firsta, 'click', firsta.flip);}
}

function change_ops(){
  
}

function op_click(){
  //$(this.myForm).change_ops();
  // set name attribute
  
}

function use_ajax(form_id, callback, div_id){
  $(form_id).onsubmit = function() {
    var cdiv = document.getElementsByClassName('comment')[0];
    new Ajax.Updater(cdiv, callback, {evalScripts:true, method:'post', postBody:Form.serialize(this)});
    return false;
  }
  var ops = Form.getInputs(form_id, 'submit', 'op');
  for(op in ops){
    op.onclick = op_click;
    op.myForm = form_id;
  }
}
